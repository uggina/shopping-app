// Shopping cart application.

// clone the project

1. clone the app using this URL - https://gitlab.com/uggina/shopping-app.git

// install the dependencies

2. go to the project folder and run this command - npm install

// run the API's in the local server

3. open command prompt  as administrator and go to the project folder and run this command - json-server --watch db.json --port 8000

// run the application

4. open vscode or any other IDE and go to the project folder and run the application by using this command - node website.js  

// result in the browser

5. the above command will give us a brower link - http://localhost:3000/